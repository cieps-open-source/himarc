/* eslint-env mocha */
/* eslint-disable no-unused-expressions */
'use strict';

const { expect } = require('chai');
const fs = require('fs');
const path = require('path');
const mrkRecord = fs.readFileSync(path.join(__dirname, './data/nature.mrk'), 'utf8');
const mrcRecord = fs.readFileSync(path.join(__dirname, './data/nature.mrc'), 'utf8');
const { mrcToObject, tokenizer, syntaxAnalyzer, toHimarc, toHTML, isFieldRepeatable } = require('../src/himarc.js');

describe('hiMarc', function () {
  describe('tokenizer()', function () {
    it('should return a array of token from a marc21 record (ISO 2709)', function () {
      const tokens = tokenizer(mrkRecord);
      expect(tokens).to.be.an('array');
      tokens.forEach(token => {
        expect(token).to.be.an('object');
        expect(token).to.have.property('type');
        expect(token.type).to.be.a('string');
        expect(token).to.have.property('value');
        expect(token.value).to.be.a('string');
        expect(token).to.have.property('startPosition');
        expect(token.startPosition).to.be.a('number');
      });
    });
  });

  describe('syntaxAnalyzer()', function () {
    it('should return a object improved by parsing', function () {
      const result = syntaxAnalyzer(tokenizer(mrkRecord));
      expect(result).to.be.an('object');
      expect(result).to.have.property('data');
      expect(result.data).to.be.an('array');
      result.data.forEach(fieldInfo => {
        expect(fieldInfo).to.be.an('object');
        expect(fieldInfo).to.have.property('type');
        expect(fieldInfo.type).to.be.a('string');
        expect(fieldInfo).to.have.property('value');
        if (fieldInfo.type === 'dataFieldInfo') {
          expect(fieldInfo.value).to.be.a('array');
          fieldInfo.value.forEach(fieldInfo => {
            expect(fieldInfo).to.be.an('object');
            expect(fieldInfo).to.have.property('type');
            expect(fieldInfo.type).to.be.a('string');
            expect(fieldInfo).to.have.property('value');
            expect(fieldInfo.value).to.be.a('string');
            expect(fieldInfo).to.have.property('startPosition');
            expect(fieldInfo.startPosition).to.be.a('number');
          });
        } else {
          expect(fieldInfo.value).to.be.a('string');
        }
        expect(fieldInfo).to.have.property('startPosition');
        expect(fieldInfo.startPosition).to.be.a('number');
      });
      expect(result).to.have.property('errors');
      expect(result.errors).to.be.an('array');
      result.errors.forEach(error => {
        expect(error).to.be.an('object');
        expect(error).to.have.property('type');
        expect(error.type).to.be.a('string');
        expect(error).to.have.property('value');
        if (error.type === 'dataFieldInfo') {
          expect(error.value).to.be.a('array');
          error.value.forEach(error => {
            expect(error).to.be.an('object');
            expect(error).to.have.property('type');
            expect(error.type).to.be.a('string');
            expect(error).to.have.property('value');
            expect(error.value).to.be.a('string');
            expect(error).to.have.property('startPosition');
            expect(error.startPosition).to.be.a('number');
          });
        } else {
          expect(error.value).to.be.a('string');
        }
        expect(error).to.have.property('startPosition');
        expect(error.startPosition).to.be.a('number');
      });
    });

    it('missing subfield delimitator should return an error', function() {
      const mrk = String.raw`=035  \\a(ISSN)00280836`
      const result = syntaxAnalyzer(tokenizer(mrk));

      expect(result.errors.length).to.be.equal(1);
      expect(result.errors[0].message).to.be.equal('parsing error: subfield delimiter not found');
    });
  });

  describe('toHimarc()', function () {
    it('should return an record Object JS next the parsing step', function () {
      const result = toHimarc(syntaxAnalyzer(tokenizer(mrkRecord)));
      const expectedResult = {
        264: [
          {
            indicator1: '3',
            indicator2: '1',
            subFields: [{ a: 'London' }, { b: 'Springer Nature Limited' }]
          }
        ],
        336: [
          {
            indicator1: '\\',
            indicator2: '\\',
            subFields: [{ a: 'text' }, { 2: 'rdacontent' }]
          }
        ],
        337: [
          {
            indicator1: '\\',
            indicator2: '\\',
            subFields: [{ a: 'unmediated' }, { 2: 'rdamedia' }]
          }
        ],
        338: [
          {
            indicator1: '\\',
            indicator2: '\\',
            subFields: [{ a: 'volume' }, { 2: 'rdacarrier' }]
          }
        ],
        510: [
          {
            indicator1: '\\',
            indicator2: '\\',
            subFields: [{ a: 'CABABSTRACTS' }, { 9: 'facet' }]
          },
          {
            indicator1: '\\',
            indicator2: '\\',
            subFields: [{ a: 'CROSSREF' }, { 9: 'facet' }]
          }
        ],
        LDR: {
          positions: {
            10: '2',
            11: '2',
            17: ' ',
            18: 'a',
            19: ' ',
            20: '4',
            21: '5',
            22: '0',
            23: '0',
            '00-04': '02105',
            '05': 'c',
            '06': 'a',
            '07': 's',
            '08': ' ',
            '09': 'a',
            '12-16': '00541'
          }
        },
        "856": [
          {
            "indicator1": "4",
            "indicator2": "1",
            "subFields": [
              {
                "u": "http://www.ub.umu.se/sok/digitaliserat-material/hushallningssallskapet/"
              },
              {
                "x": "test"
              }
            ]
          }
        ],
        880: [
          {
            indicator1: '\\',
            indicator2: '\\',
            subFields: [
              {
                6: '222-01/$1another title'
              }
            ]
          }
        ],
        '001': '0028-0836',
        '003': 'ISSN',
        '005': '20190817162300.0',
        '007': [{
          positions: {
            10: 'p',
            11: 'a',
            12: 'b',
            13: 'p',
            '00': 'c',
            '01': 'u',
            '02': ' ',
            '03': 'g',
            '04': 'n',
            '05': ' ',
            '06-08': '008',
            '09': 'a'
          }
        }],
        '008': {
          positions: {
            18: 'w',
            19: 'r',
            20: '|',
            21: 'p',
            22: ' ',
            23: ' ',
            24: ' ',
            25: ' ',
            26: ' ',
            27: ' ',
            28: ' ',
            29: '0',
            33: 'a',
            34: '0',
            38: ' ',
            39: ' ',
            '00-05': '190816',
            '06': 'c',
            '07-10': '1869',
            '11-14': '9999',
            '15-17': 'enk',
            '30-32': '   ',
            '35-37': 'eng'
          }
        },
        '035': [
          {
            indicator1: '\\',
            indicator2: '\\',
            subFields: [{ a: '(ISSN)00280836' }]
          }
        ],
        '039': {
          indicator1: '\\',
          indicator2: '9',
          subFields: [
            { a: '201908171623' },
            { b: 'VLOAD' },
            { c: '201908171559' },
            { d: 'VLOAD' },
            { c: '201908161254' },
            { d: 'ICBIB' },
            { c: '201905041728' },
            { d: 'VLOAD' },
            { y: '200406091632' },
            { z: 'load' },
            { w: 'GBR_20190816.mrc' },
            { x: '3' }
          ]
        },
        '044': { indicator1: '\\', indicator2: '\\', subFields: [{ c: 'GBR' }] },
        ABC: [{
          indicator1: '1',
          indicator2: '2',
          subFields: [{ a: 'Lorem' }, { B: 'Ipsum' }, { c: 'Dolor' }]
        }]
      };
      const expectedErrors = [
        { type: 'unknown', value: 'tu quoque mi fili', startPosition: 465 },
        {
          type: 'tag',
          value: 'ABC',
          startPosition: 660,
          message: 'tag field is invalid'
        },
        {
          type: 'indicators',
          value: '123',
          startPosition: 665,
          message: 'Indicators must have two characters in every variable data field'
        },
        { type: 'field', value: '044', message: "field is repeated when it shouldn't" }
      ];
      expect(result.fields).to.be.deep.equal(expectedResult);
      expect(result.errors).to.be.deep.equal(expectedErrors);
    });
  });

  describe('mrcToObject()', function () {
    it('should return a record Object JS', function () {
      const result = mrcToObject(mrcRecord);
      const expectedResult = {
        264: [
          {
            indicator1: '3',
            indicator2: '1',
            subFields: [{ a: 'London' }, { b: 'Springer Nature Limited' }]
          }
        ],
        336: [
          {
            indicator1: '\\',
            indicator2: '\\',
            subFields: [{ a: 'text' }, { 2: 'rdacontent' }]
          }
        ],
        337: [
          {
            indicator1: '\\',
            indicator2: '\\',
            subFields: [{ a: 'unmediated' }, { 2: 'rdamedia' }]
          }
        ],
        338: [
          {
            indicator1: '\\',
            indicator2: '\\',
            subFields: [{ a: 'volume' }, { 2: 'rdacarrier' }]
          }
        ],
        510: [
          {
            indicator1: '\\',
            indicator2: '\\',
            subFields: [{ a: 'CABABSTRACTS' }, { 9: 'facet' }]
          },
          {
            indicator1: '\\',
            indicator2: '\\',
            subFields: [{ a: 'CROSSREF' }, { 9: 'facet' }]
          }
        ],
        LDR: {
          positions: {
            10: '2',
            11: '2',
            17: ' ',
            18: 'a',
            19: ' ',
            20: '4',
            21: '5',
            22: '0',
            23: '0',
            '00-04': '00717',
            '05': 'c',
            '06': 'a',
            '07': 's',
            '08': ' ',
            '09': 'a',
            '12-16': '00217'
          }
        },
        '001': '0028-0836',
        '003': 'ISSN',
        '005': '20190817162300.0',
        '007': [{
          positions: {
            10: 'p',
            11: 'a',
            12: 'b',
            13: 'p',
            '00': 'c',
            '01': 'u',
            '02': ' ',
            '03': 'g',
            '04': 'n',
            '05': ' ',
            '06-08': '008',
            '09': 'a'
          }
        }],
        '008': {
          positions: {
            18: 'w',
            19: 'r',
            20: '|',
            21: 'p',
            22: ' ',
            23: ' ',
            24: ' ',
            25: ' ',
            26: ' ',
            27: ' ',
            28: ' ',
            29: '0',
            33: 'a',
            34: '0',
            38: ' ',
            39: ' ',
            '00-05': '190816',
            '06': 'c',
            '07-10': '1869',
            '11-14': '9999',
            '15-17': 'enk',
            '30-32': '   ',
            '35-37': 'eng'
          }
        },
        '035': [
          {
            indicator1: '\\',
            indicator2: '\\',
            subFields: [{ a: '(ISSN)00280836' }]
          }
        ],
        '039': {
          indicator1: '\\',
          indicator2: '9',
          subFields: [
            { a: '201908171623' },
            { b: 'VLOAD' },
            { c: '201908171559' },
            { d: 'VLOAD' },
            { c: '201908161254' },
            { d: 'ICBIB' },
            { c: '201905041728' },
            { d: 'VLOAD' },
            { y: '200406091632' },
            { z: 'load' },
            { w: 'GBR_20190816.mrc' },
            { x: '3' }
          ]
        },
        '044': { indicator1: '\\', indicator2: '\\', subFields: [{ c: 'GBR' }] }, 
        "ABC": [{
          indicator1: '1',
          indicator2: '2',
          subFields: [{ a: 'Lorem' }, { B: 'Ipsum' }, { c: 'Dolor' }]
        }],
        "856": [
          {
            "indicator1": "4",
            "indicator2": "1",
            "subFields": [
              {
                "u": "http://www.ub.umu.se/sok/digitaliserat-material/hushallningssallskapet/"
              },
              {
                "x": "test"
              }
            ]
          }
        ]
      };
      expect(result).to.be.deep.equal(expectedResult);
    });
  });

  describe('toHTML()', function () {
    it('sould return a valid HTML representation of record', function() {
      const analyzer = syntaxAnalyzer(tokenizer(mrkRecord));
      const result = toHTML(analyzer.data);
      const expected = String.raw`<span class="startField">=</span><span class="tag">LDR</span>  <span class="controlFieldInfo">02105cas a2200541 a 4500</span>
<span class="startField">=</span><span class="tag">001</span>  <span class="controlFieldInfo">0028-0836</span>
<span class="startField">=</span><span class="tag">003</span>  <span class="controlFieldInfo">ISSN</span>
<span class="startField">=</span><span class="tag">005</span>  <span class="controlFieldInfo">20190817162300.0</span>
<span class="startField">=</span><span class="tag">007</span>  <span class="controlFieldInfo">cu gn 008apabp</span>
<span class="startField">=</span><span class="tag">008</span>  <span class="controlFieldInfo">190816c18699999enkwr|p       0   a0eng  </span>
<span class="startField">=</span><span class="tag">035</span>  <span class="indicators">\\</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">a</span>(ISSN)00280836
<span class="startField">=</span><span class="tag">039</span>  <span class="indicators">\9</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">a</span>201908171623<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">b</span>VLOAD<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">c</span>201908171559<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">d</span>VLOAD<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">c</span>201908161254<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">d</span>ICBIB<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">c</span>201905041728<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">d</span>VLOAD<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">y</span>200406091632<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">z</span>load<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">w</span>GBR_20190816.mrc<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">x</span>3
<span class="startField">=</span><span class="tag">044</span>  <span class="indicators">\\</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">c</span>ZNN
<span class="startField">=</span><span class="tag">044</span>  <span class="indicators">\\</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">c</span>GBR
<span class="startField">=</span><span class="tag">264</span>  <span class="indicators">31</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">a</span>London<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">b</span>Springer Nature Limited
<span class="startField">=</span><span class="tag">336</span>  <span class="indicators">\\</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">a</span>text<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">2</span>rdacontent
<span class="startField">=</span><span class="tag">337</span>  <span class="indicators">\\</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">a</span>unmediated<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">2</span>rdamedia
<span class="startField">=</span><span class="tag">338</span>  <span class="indicators">\\</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">a</span>volume<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">2</span>rdacarrier
<span class="unknown">tu quoque mi fili</span>
<span class="startField">=</span><span class="tag">510</span>  <span class="indicators">\\</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">a</span>CABABSTRACTS<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">9</span>facet
<span class="startField">=</span><span class="tag">510</span>  <span class="indicators">\\</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">a</span>CROSSREF<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">9</span>facet
<span class="startField">=</span><span class="tag">856</span>  <span class="indicators">41</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">u</span>http://www.ub.umu.se/sok/digitaliserat-material/hushallningssallskapet/<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">x</span>test
<span class="startField">=</span><span class="tag">880</span> <span class="indicators">\\</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">6</span>222-01/$1another title
<span class="startField">=</span><span class="tag">ABC</span>  <span class="indicators">123</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">a</span>Lorem<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">B</span>Ipsum<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">c</span>Dolor`;
      expect(result).to.be.equal(expected);
    });

    it('should escape html characters in fields', function() {
      const analyzer = syntaxAnalyzer(tokenizer(mrkRecord));
      analyzer.data = analyzer.data.map(elt => {
        // create fake html content in 246 field value
        if (Array.isArray(elt.value) && elt.value[1].value === 'Springer Nature Limited') {
          elt.value[1].value = 'Springer & Nature Limit<ed>';
        }
        return elt
      });
      const result = toHTML(analyzer.data);
      const expected = String.raw`<span class="startField">=</span><span class="tag">LDR</span>  <span class="controlFieldInfo">02105cas a2200541 a 4500</span>
<span class="startField">=</span><span class="tag">001</span>  <span class="controlFieldInfo">0028-0836</span>
<span class="startField">=</span><span class="tag">003</span>  <span class="controlFieldInfo">ISSN</span>
<span class="startField">=</span><span class="tag">005</span>  <span class="controlFieldInfo">20190817162300.0</span>
<span class="startField">=</span><span class="tag">007</span>  <span class="controlFieldInfo">cu gn 008apabp</span>
<span class="startField">=</span><span class="tag">008</span>  <span class="controlFieldInfo">190816c18699999enkwr|p       0   a0eng  </span>
<span class="startField">=</span><span class="tag">035</span>  <span class="indicators">\\</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">a</span>(ISSN)00280836
<span class="startField">=</span><span class="tag">039</span>  <span class="indicators">\9</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">a</span>201908171623<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">b</span>VLOAD<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">c</span>201908171559<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">d</span>VLOAD<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">c</span>201908161254<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">d</span>ICBIB<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">c</span>201905041728<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">d</span>VLOAD<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">y</span>200406091632<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">z</span>load<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">w</span>GBR_20190816.mrc<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">x</span>3
<span class="startField">=</span><span class="tag">044</span>  <span class="indicators">\\</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">c</span>ZNN
<span class="startField">=</span><span class="tag">044</span>  <span class="indicators">\\</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">c</span>GBR
<span class="startField">=</span><span class="tag">264</span>  <span class="indicators">31</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">a</span>London<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">b</span>Springer &amp; Nature Limit&lt;ed&gt;
<span class="startField">=</span><span class="tag">336</span>  <span class="indicators">\\</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">a</span>text<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">2</span>rdacontent
<span class="startField">=</span><span class="tag">337</span>  <span class="indicators">\\</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">a</span>unmediated<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">2</span>rdamedia
<span class="startField">=</span><span class="tag">338</span>  <span class="indicators">\\</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">a</span>volume<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">2</span>rdacarrier
<span class="unknown">tu quoque mi fili</span>
<span class="startField">=</span><span class="tag">510</span>  <span class="indicators">\\</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">a</span>CABABSTRACTS<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">9</span>facet
<span class="startField">=</span><span class="tag">510</span>  <span class="indicators">\\</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">a</span>CROSSREF<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">9</span>facet
<span class="startField">=</span><span class="tag">856</span>  <span class="indicators">41</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">u</span>http://www.ub.umu.se/sok/digitaliserat-material/hushallningssallskapet/<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">x</span>test
<span class="startField">=</span><span class="tag">880</span> <span class="indicators">\\</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">6</span>222-01/$1another title
<span class="startField">=</span><span class="tag">ABC</span>  <span class="indicators">123</span><span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">a</span>Lorem<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">B</span>Ipsum<span class="subFieldCodeDelimiter">$</span><span class="subFieldCode">c</span>Dolor`;
      expect(result).to.be.equal(expected);
    });
  });

  describe('isFieldRepeatable()', function () {
    ["022","222", "039", "044", "699"].forEach(function (tag) {
      it(`${tag} should not be considered repeatable`, function () {
        const repeatable = isFieldRepeatable(tag);
        expect(repeatable).to.be.false;
      });
    })
  });
});
